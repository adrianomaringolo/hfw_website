var gulp = require('gulp'),
    imagemin = require('gulp-imagemin'),
    clean = require('gulp-clean'),
    uglify = require('gulp-uglify'),
    cssmin = require('gulp-cssmin'),
    usemin = require('gulp-usemin'),
    jshint = require('gulp-jshint'),
    jshintStylish = require('jshint-stylish'),
    csslint = require('gulp-csslint'),
    autoprefixer = require('gulp-autoprefixer'),
    fileinclude = require('gulp-file-include'),
    less = require('gulp-less'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload;

const DEV = 'development';
const PROD = 'production';
var environment = '';

function getDestFolder() {
    return (environment == PROD) ? 'dist' : 'dev';
}

gulp.task('distribute', ['copy'], function() {
    environment = PROD;
    gulp.start('build-img', 'fileinclude', 'usemin');
})

gulp.task('build-img', function() {
    gulp.src('dist/img/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'));
});

gulp.task('copy', ['clean'], function() {
    return gulp.src('src/**/*')
        .pipe(gulp.dest(getDestFolder()));
});

gulp.task('clean', function() {
    return gulp.src(getDestFolder())
        .pipe(clean());
});

gulp.task('usemin', function() {
    gulp.src('dist/**/*.html')
        .pipe(usemin({
            js: [uglify],
            css: [autoprefixer, cssmin]
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('fileinclude', function() {
    gulp.src(['src/index.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest(getDestFolder()));
});


// tiny static page generator
gulp.task('fileinclude-build', ['copy'], function() {
    return gulp.src(['src/index.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('dev'))
        .pipe(browserSync.stream());
});
// process JS files and return to the stream
gulp.task('js', function() {
    return gulp.src('src/**/*.js')
        .pipe(gulp.dest('dev'))
        .pipe(browserSync.stream());
});
// process CSS files and return to the stream
gulp.task('css', function() {
    return gulp.src('src/**/*.css')
        .pipe(gulp.dest('dev'))
        .pipe(browserSync.stream());
});

// ensures these tasks are completed before reloading browsers
gulp.task('js-watch', ['js'], reload);
gulp.task('include-watch', ['fileinclude-build'], reload);

gulp.task('default', ['fileinclude-build'], function() {
    // serve files from the build folder
    browserSync.init({
        server: {
            baseDir: "dev"
        }
    });
    // watch files and run tasks
    gulp.watch("src/**/*.html", ['include-watch']);
    gulp.watch("src/**/*.js", ['js-watch']);
    gulp.watch("src/**/*.css", ['css']);
});

'use strict';

const express = require('express');
const bodyParser = require('body-parser')
const MongoClient = require('mongodb').MongoClient
const nodemailer = require('nodemailer');
const app = express();

app.set('port', (process.env.PORT || 5000));
app.use(express.static('src'));
app.use(bodyParser.urlencoded({
  extended: true
}))
app.set('views', __dirname + '/src/views');
app.set('view engine', 'ejs')

var db

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
  host: 'webmail.hospitalhacos.org.br',
  port: '587',
  auth: {
    user: 'eventos@hospitalhacos.org.br',
    pass: 'EVhOsP2013'
  },
  tls: {
    rejectUnauthorized: false
  }
});

// setup email data with unicode symbols
let mailOptions = {
  from: '"Eventos Hospitalhaços" <eventos@hospitalhacos.org.br>', // sender address
  subject: 'HFW - Confirmação de presença', // Subject line
  text: 'Obrigado por confirmar sua presença para o evento do dia 10/05!', // plain text body
  html: '<img style="background-color: rgba(0, 0, 0, 0.57); display: block; width: 428.5px; height: 171.5px; margin-bottom: 25px; padding: 20px;" src="https://hfw-hospitalhacos.herokuapp.com/assets/img/logo.png">' +
        '<b>Agradecemos a sua confirmação!</b>' +
        '<br/><br/>O evento será realizado no dia 10/05, no Tênis Clube de Campinas, a partir das 19h.' +
        '<br/>Endereço: Rua Coronel Quirino, 1346 - Cambuí' +
        '<br/>Informações: (19) 3237-2603 ou eventos@hospitalhacos.org.br' +
        '<br/><br/>Att.,<br/><i>Comissão Organizadora</i>'
};

MongoClient.connect('mongodb://hospitalhacos:hfw2017@ds135800.mlab.com:35800/hfw-hotsite', (err, database) => {
  if (err) return console.log(err)
  db = database
  app.listen(app.get('port'), () => {
    console.log('listening on ' + app.get('port'))
  })
})

app.get('/', function (req, res) {
  res.render(__dirname + '/src/index.ejs', {
    anchor: '',
    error: '',
    success: ''
  })
})

app.get('/confirmation-panel', function (req, res) {
  db.collection('confirmations').find().toArray(function (err, result) {
    res.render('panel.ejs', {
      confirmations: result
    })
  })
})

app.post('/confirmation', (req, res) => {
  req.body.confirmation_date = new Date();

  db.collection('codes').findOne({
    "code": req.body.codigo
  }, function (err, document) {
    console.log(document);
    if (document != null) { // found the code - OK
      console.log(document);

      db.collection('confirmations').findOne({
        "email": req.body.email.toLowerCase()
      }, function (err, person) {

        if (person == null) { // email not found - OK

          req.body.email = req.body.email.toLowerCase();

          db.collection('confirmations').save(req.body, (err, result) => {
            if (err) return console.log(err)
            mailOptions.to = req.body.email;

            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                return console.log(error);
              }
              console.log('Message %s sent: %s', info.messageId, info.response);
            });

            console.log('saved to database')
            res.render(__dirname + '/src/index.ejs', {
              anchor: 'confirmacao',
              error: '',
              success: 'Sua confirmação foi enviada! Em breve você receberá um email com as informações.'
            })

          })

        } else {
          res.render(__dirname + '/src/index.ejs', {
            anchor: 'confirmacao',
            error: 'Esse email já foi confirmado!',
            success: ''
          })
        }
      })
    } else {
      res.render(__dirname + '/src/index.ejs', {
        anchor: 'confirmacao',
        error: 'O código digitado não é valido!',
        success: ''
      })
    }
  });


})
